var cr8 = function(){
    var _this = this;
	var data = null,
		params = null;

    _this.response = {status: 500};

    this.setData = function(dataCollection){
        this.data = dataCollection;
    };

    this.setParams = function(paramsCollection){
        this.params = paramsCollection;
    };

    this.getParams = function(){
        console.log(this.params);
    };

    this.send = function(call){
        if(!this.params.url) {
            console.error("cr8: URL parameter of type string not set using cr8.setParams(\"url\") property.");
            _this.response = {status: 500, message: "Internal Server Error"};
            return _this.response;
        }

        if(typeof (this.data) !== "undefined" && Object.keys(this.data).length > 0) {
            $.ajax({
                type: "POST",
                url: this.params.url,
                dataType: "json",
                data: this.data,
                success: function(res) {
                    _this.response = res;
                    if(_this.response.status === 200){
                        if(call.hasOwnProperty('success') && $.isFunction(call.success)) {
                            call.success(_this.response);
                        }
                    } else {
                        if(call.hasOwnProperty('error') && $.isFunction(call.error)) {
                            call.error(_this.response);
                        }
                    }
                },
                error: function(jqxhr, textStatus, error) {
                    _this.response = {status: 404, message: "Could not contact server."};
                }
            });
        } else {
            console.error("cr8: Data collection of type object was not set using cr8.setData() property.");
            _this.response = {status: 500, message: "Internal Server Error"};
            return _this.response;
        }
    };

    this.getResponse = function() {
        if (typeof _this.response === "object")
            return _this.response;
        else
            console.error("cr8: Unexpected type \"" + typeof response + "\" found while expecting an object." );
    };
};