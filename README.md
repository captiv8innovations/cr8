cr8 (crate)
===================
cr8 is an AJAX javascript class that encapsulates data as JSON in a clean, functional and formatted way to POST to a web service.

## Terms of Use
cr8 is created and maintained by [captiv8 Innovations Inc](https://captiv8innovations.com). The code is available under the [captiv8 Terms of Use License](https://captiv8innovations.com/terms).